const texts = document.querySelectorAll('.content-tab li');
const sctab = document.querySelector('.manyTab');
sctab.addEventListener('click', function (event) {
    const target = event.target.dataset.tabName;
    event.target.closest('ul').querySelector('.active').classList.remove('active');
    event.target.classList.add('active');
    texts.forEach(item => {
        item.setAttribute('hidden', true);
        if (target === item.dataset.tabText) {
            item.removeAttribute('hidden');
        }
    })
})
const filterButtons = document.querySelector("#filter-btns").children;
const items = document.querySelector(".portfolio-gallery").children;
for (let i = 0; i < filterButtons.length; i++) {
    filterButtons[i].addEventListener("click", function () {
        for (let j = 0; j < filterButtons.length; j++) {
            filterButtons[j].classList.remove("active-second")
        } 
        this.classList.add("active-second");
        const target = this.getAttribute("data-target")
        for (let k = 0; k < items.length; k++) {
            items[k].style.display = "none";
            if (target == items[k].getAttribute("data-id")) {
                items[k].style.display = "block";
            }
            if (target === "all") {
                items[k].style.display = "block";
            }
        }
    })
}
let loadMore = document.querySelector('.tabs-button');
let displaySc = document.querySelectorAll("[data-data='load-more']")
loadMore.addEventListener('click', function (event) {
    const target = this.getAttribute("data-toggle")
        for (let k = 0; k < items.length; k++) {
            if (target == items[k].getAttribute("data-data")) {
                items[k].style.display = "block";
            }   
}
loadMore.style.display = "none";
})
