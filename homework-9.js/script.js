const texts = document.querySelectorAll('.content-tab li');
const sctab = document.querySelector('.manyTab');
sctab.addEventListener('click', function (event) {
    event.target.classList.add('active');
    const target = event.target.dataset.tabName;
    event.target.closest('ul').querySelector('.active').classList.remove('active');
    
    texts.forEach(item => {
        item.setAttribute('hidden', true);
        if (target === item.dataset.tabText) {
            item.removeAttribute('hidden');
        }
    })
})
